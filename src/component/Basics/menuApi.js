const Menu  = [{
    id: 1,
    image: "images/fruitsalad.jpg",
    name: "Fruit salad",
    category: "lunch",
    price:30,
    description: "Fruit salad is best that you can have here "
},
{
    id: 2,
    image: "images/bakedzitiwithmeat.jpg",
    name: "Baked Ziti With Meat",
    category: "dinner",
    price:30,
    description: "Meat is best that you can have here "
},
{
    id: 3,
    image: "images/food.jpg",
    name: "Combo food",
    category: "lunch",
    price:30,
    description: "Combo food is best that you can have here "
},
{
    id: 4,
    image: "images/smoothie.jpg",
    name: "Smoothie",
    category: "breakfast",
    price:30,
    description: "Smoothie is best that you can have here "
},
{
    id: 5,
    image: "images/popcorn.jpg",
    name: "Popcorn",
    category: "evening",
    price:30,
    description: "popcorn is best that you can have here "
},
{
    id: 6,
    image: "images/maggi.jpg",
    name: "Maggi",
    category: "breakfast",
    price:30,
    description: "I love maggi best that you can have here"
},
{
    id: 7,
    image: "images/capresesandwich.jpg",
    name: "Caprese sandwich",
    category: "lunch",
    price:30,
    description: "Caprese sandwich is best that you can have here "
},
{
    id: 8,
    image: "images/greenchickensalad.jpg",
    name: "Greenchickensalad",
    category: "lunch",
    price:30,
    description: "Greenchickensalad is best that you can have here "
},
{
    id: 9,
    image: "images/popcorn.jpg",
    name: "Popcorn",
    category: "evening",
    price:30,
    description: "Popcorn is best that you can have here "
},
{
    id: 10,
    image: "images/food.jpg",
    name: "Combo food",
    category: "lunch",
    price:30,
    description: "Combo food is best that you can have here "
},
{
    id: 11,
    image: "images/greenchickensalad.jpg",
    name: "Greenchickensalad",
    category: "lunch",
    price:30,
    description: "Greenchickensalad is best that you can have here "
},
{
    id: 12,
    image: "images/tea.jpg",
    name: "Tea",
    category: "evening",
    price:30,
    description: "Tea is best that you can have here "
},
{
    id: 13,
    image: "images/mushroom.jpg",
    name: "Mushroom",
    category: "dinner",
    price:30,
    description: "Mushroom is best that you can have here "
},
{
    id: 14,
    image: "images/tea.jpg",
    name: "Tea",
    category: "evening",
    price:30,
    description: "Tea is best that you can have here "
},
{
    id: 15,
    image: "images/curry.jpg",
    name: "Lentils vegetable curry",
    category: "dinner",
    price:30,
    description: "Curry is best that you can have here "
},
{
    id: 16,
    image: "images/pizza.jpg",
    name: "Pizza",
    category: "evening",
    price:30,
    description: "Pizza is best that you can have here "
},
{
    id: 17,
    image: "images/pizza.jpg",
    name: "Specials",
    category: "Specials",
    price:30,
    description: "Specials is unique and the best that you can have here "
},
];

export default Menu;