import React, { useState } from 'react'
import "./style.css"
import Menu from "./menuApi.js"
import MenuCard from './MenuCard'
import Navbar from './Navbar'

const uniqueList = [...new Set(Menu.map((curElem)=>{
  return curElem.category;
})
),
"Full Menu",
];

console.log(uniqueList);

const Restaurant = () => {

   const [menuData, setmenuData]  = useState(Menu);
   const [menuList,setmenuList] = useState(uniqueList);


  
   const filterItem = (category) =>{

    if(category==="Full Menu"){
      setmenuData(Menu);
      return;
    }

    const updated_list = Menu.filter((curElem)=>{
      return curElem.category ===category;
    });

    setmenuData(updated_list);

   };
   
  return (
    <>
      <Navbar filterItem={filterItem} menuList={menuList}/>
      <MenuCard menuData={menuData} />
    </>
  )
}

export default Restaurant
